/*
 * TwoWire.cpp - TWI/I2C Library for Wiring & Arduino
 * Copyright (c) 2006 Nicholas Zambetti. All rights reserved.
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 * 
 * Modified 2012 by Todd Krein (todd@krein.org) to implement repeated starts.
 */

extern "C" {
  #include <stdlib.h>
  #include <string.h>
  #include <inttypes.h>
  #include "twi.h"
}

#include "Wire.h"

// Initialize Class Variables

uint8_t TwoWire::rxBuffer[BUFFER_LENGTH];
uint8_t TwoWire::rxBufferIndex = 0;
uint8_t TwoWire::rxBufferLength = 0;

uint8_t TwoWire::txAddress = 0;
uint8_t TwoWire::txBuffer[BUFFER_LENGTH];
uint8_t TwoWire::txBufferIndex = 0;
uint8_t TwoWire::txBufferLength = 0;

uint8_t TwoWire::transmitting = 0;
void (* TwoWire::user_onRequest)(void);
void (* TwoWire::user_onReceive)(int);

// Constructors

TwoWire::TwoWire() {}

// Public Methods

/*@ assigns
  @   rxBufferIndex,
  @   rxBufferLength,
  @   txBufferIndex,
  @   txBufferLength;
  @ ensures
  @      rxBufferIndex  == 0
  @   && rxBufferLength == 0
  @   && txBufferIndex  == 0
  @   && txBufferLength == 0;
  @*/
void TwoWire::begin(void)
{
  rxBufferIndex = 0;
  rxBufferLength = 0;

  txBufferIndex = 0;
  txBufferLength = 0;

  twi_init();
}

/*@ requires
  @   0 <= address < 128;
  @ assigns
  @   rxBufferIndex,
  @   rxBufferLength,
  @   txBufferIndex,
  @   txBufferLength;
  @ ensures
  @      rxBufferIndex  == 0
  @   && rxBufferLength == 0
  @   && txBufferIndex  == 0
  @   && txBufferLength == 0;
  @*/
void TwoWire::begin(uint8_t address)
{
  twi_setAddress(address);
  twi_attachSlaveTxEvent(onRequestService);
  twi_attachSlaveRxEvent(onReceiveService);
  begin();
}

/*@ requires
  @   0 <= address < 128;
  @ assigns
  @   rxBufferIndex,
  @   rxBufferLength,
  @   txBufferIndex,
  @   txBufferLength;
  @ ensures
  @      rxBufferIndex  == 0
  @   && rxBufferLength == 0
  @   && txBufferIndex  == 0
  @   && txBufferLength == 0;
  @*/
void TwoWire::begin(int address)
{
  begin((uint8_t) address);
}

/*@ assigns
  @   \nothing;
  @*/
void TwoWire::end(void)
{
  twi_disable();
}

/*@ requires
  @      clock == 100000
  @   || clock == 400000;
  @ assigns
  @   \nothing;
  @*/
void TwoWire::setClock(uint32_t clock)
{
  twi_setFrequency(clock);
}

/*@ requires
  @      0 <= address < 128
  @   && 0 < quantity <= BUFFER_LENGTH
  @   && 0 <= iaddress < 16777216
  @   && (sendStop == 1 || sendStop == 0);
  @ assigns
  @   transmitting,
  @   txAddress,
  @   txBuffer[0 .. BUFFER_LENGTH - 1],
  @   txBufferIndex,
  @   txBufferLength,
  @   rxBuffer[0 .. BUFFER_LENGTH - 1],
  @   rxBufferIndex,
  @   rxBufferLength;
  @ ensures
  @   \result == rxBufferLength;
  @ ensures
  @     (rxBufferLength == 0 || rxBufferLength <= quantity)
  @   && rxBufferIndex  == 0;
  @ behavior internal:
  @   assumes
  @     isize > 0;
  @   assigns
  @     transmitting,
  @     txAddress,
  @     txBuffer[0 .. BUFFER_LENGTH - 1],
  @     txBufferIndex,
  @     txBufferLength,
  @     rxBuffer[0 .. BUFFER_LENGTH - 1],
  @     rxBufferIndex,
  @     rxBufferLength;
  @   ensures
  @        transmitting   == 0
  @     && txAddress      == \old(address)
  @     && txBufferIndex  == 0
  @     && txBufferLength == 0;
  @ behavior regular:
  @   assumes
  @     isize == 0;
  @   assigns
  @     rxBuffer[0 .. BUFFER_LENGTH - 1],
  @     rxBufferIndex,
  @     rxBufferLength;
  @ complete behaviors
  @   internal, regular;
  @ disjoint behaviors
  @   internal, regular;
  @*/
uint8_t TwoWire::requestFrom(uint8_t address, uint8_t quantity, uint32_t iaddress, uint8_t isize, uint8_t sendStop)
{
  if (isize > 0) {

    /*
     * Send internal address; this mode allows sending a repeated start to access
     * some devices internal registers. This function is executed by the hardware
     * TWI module on other processors (for example, Due's TWI_IADR and TWI_MMR registers).
     */

    beginTransmission(address);

    // the maximum size of internal address is 3 bytes
    if (isize > 3) {
      isize = 3;
    }

    uint8_t auxiliary = isize;

    // write internal register address - most significant byte first

    /*@ loop invariant
      @      0 <= isize <= auxiliary
      @   && txBufferIndex  == auxiliary - isize
      @   && txBufferLength == txBufferIndex
      @   && \forall uint8_t index ; 0 <= index < auxiliary - isize ==> txBuffer[index] == ((uint8_t) (iaddress >> ((auxiliary - index - 1) * 8)));
      @ loop assigns
      @   isize,
      @   txBuffer[0 .. BUFFER_LENGTH - 1],
      @   txBufferIndex,
      @   txBufferLength;
      @ loop variant
      @   isize;
      @*/
    while (isize-- > 0)
      TwoWire::write((uint8_t) (iaddress >> (isize * 8)));
    
    endTransmission(false);
  }

  // clamp to buffer length
  if (quantity > BUFFER_LENGTH) {
    quantity = BUFFER_LENGTH;
  }

  // perform blocking read into buffer
  uint8_t read = twi_readFrom(address, rxBuffer, quantity, sendStop);

  // set rx buffer iterator variables
  rxBufferIndex = 0;
  rxBufferLength = read;

  return read;
}

/*@ requires
  @      0 <= address < 128
  @   && 0 < quantity <= BUFFER_LENGTH
  @   && (sendStop == 1 || sendStop == 0);
  @ assigns
  @   rxBuffer[0 .. BUFFER_LENGTH - 1],
  @   rxBufferIndex,
  @   rxBufferLength;
  @ ensures
  @   \result == rxBufferLength;
  @ ensures
  @     (rxBufferLength == 0 || rxBufferLength <= quantity)
  @   && rxBufferIndex  == 0;
  @*/
uint8_t TwoWire::requestFrom(uint8_t address, uint8_t quantity, uint8_t sendStop)
{
  return requestFrom((uint8_t) address, (uint8_t) quantity, (uint32_t) 0, (uint8_t) 0, (uint8_t) sendStop);
}

/*@ requires
  @      0 <= address < 128
  @   && 0 < quantity <= BUFFER_LENGTH;
  @ assigns
  @   rxBuffer[0 .. BUFFER_LENGTH - 1],
  @   rxBufferIndex,
  @   rxBufferLength;
  @ ensures
  @   \result == rxBufferLength;
  @ ensures
  @     (rxBufferLength == 0 || rxBufferLength <= quantity)
  @   && rxBufferIndex  == 0;
  @*/
uint8_t TwoWire::requestFrom(uint8_t address, uint8_t quantity)
{
  return requestFrom((uint8_t) address, (uint8_t) quantity, (uint8_t) true);
}

/*@ requires
  @      0 <= address < 128
  @   && 0 < quantity <= BUFFER_LENGTH;
  @ assigns
  @   rxBuffer[0 .. BUFFER_LENGTH - 1],
  @   rxBufferIndex,
  @   rxBufferLength;
  @ ensures
  @   \result == rxBufferLength;
  @ ensures
  @     (rxBufferLength == 0 || rxBufferLength <= quantity)
  @   && rxBufferIndex  == 0;
  @*/
uint8_t TwoWire::requestFrom(int address, int quantity)
{
  return requestFrom((uint8_t) address, (uint8_t) quantity, (uint8_t) true);
}

/*@ requires
  @      0 <= address < 128
  @   && 0 < quantity <= BUFFER_LENGTH
  @   && (sendStop == 1 || sendStop == 0);
  @ assigns
  @   rxBuffer[0 .. BUFFER_LENGTH - 1],
  @   rxBufferIndex,
  @   rxBufferLength;
  @ ensures
  @   \result == rxBufferLength;
  @ ensures
  @     (rxBufferLength == 0 || rxBufferLength <= quantity)
  @   && rxBufferIndex  == 0;
  @*/
uint8_t TwoWire::requestFrom(int address, int quantity, int sendStop)
{
  return requestFrom((uint8_t) address, (uint8_t) quantity, (uint8_t) sendStop);
}

/*@ requires
  @   0 <= address < 128;
  @ assigns
  @   transmitting,
  @   txAddress,
  @   txBufferIndex,
  @   txBufferLength;
  @ ensures
  @      transmitting   == 1
  @   && txAddress      == \old(address)
  @   && txBufferIndex  == 0
  @   && txBufferLength == 0;
  @*/
void TwoWire::beginTransmission(uint8_t address)
{
  // indicate that we are transmitting
  transmitting = 1;

  // set address of targeted slave
  txAddress = address;

  // reset tx buffer iterator variables
  txBufferIndex = 0;
  txBufferLength = 0;
}

/*@ requires
  @   0 <= address < 128;
  @ assigns
  @   transmitting,
  @   txAddress,
  @   txBufferIndex,
  @   txBufferLength;
  @ ensures
  @      transmitting   == 1
  @   && txAddress      == \old(address)
  @   && txBufferIndex  == 0
  @   && txBufferLength == 0;
  @*/
void TwoWire::beginTransmission(int address)
{
  beginTransmission((uint8_t) address);
}

/*
 * Originally, 'endTransmission' was an f(void) function.
 * It has been modified to take one parameter indicating
 * whether or not a STOP should be performed on the bus.
 * Calling endTransmission(false) allows a sketch to
 * perform a repeated start.
 * 
 * WARNING: Nothing in the library keeps track of whether
 * the bus tenure has been properly ended with a STOP. It
 * is very possible to leave the bus in a hung state if
 * no call to endTransmission(true) is made. Some I2C
 * devices will behave oddly if they do not see a STOP.
 */

/*@ requires
  @      0 <= txAddress < 128
  @   && 0 < txBufferLength <= BUFFER_LENGTH
  @   && (sendStop == 1 || sendStop == 0);
  @ assigns
  @   txBufferIndex,
  @   txBufferLength,
  @   transmitting;
  @ ensures
  @      \result == 0
  @   || \result == 1
  @   || \result == 2
  @   || \result == 3
  @   || \result == 4;
  @ ensures
  @      txBufferIndex  == 0
  @   && txBufferLength == 0
  @   && transmitting   == 0;
  @*/
uint8_t TwoWire::endTransmission(uint8_t sendStop)
{
  // transmit buffer (blocking)
  uint8_t result = twi_writeTo(txAddress, txBuffer, txBufferLength, 1, sendStop);
  
  // reset tx buffer iterator variables
  txBufferIndex = 0;
  txBufferLength = 0;

  // indicate that we are done transmitting
  transmitting = 0;

  return result;
}

/*
 * This provides backwards compatibility with the original
 * definition, and expected behaviour, of 'endTransmission'.
 */

/*@ requires
  @      0 <= txAddress < 128
  @   && 0 < txBufferLength <= BUFFER_LENGTH;
  @ assigns
  @   txBufferIndex,
  @   txBufferLength,
  @   transmitting;
  @ ensures
  @      \result == 0
  @   || \result == 1
  @   || \result == 2
  @   || \result == 3
  @   || \result == 4;
  @ ensures
  @      txBufferIndex  == 0
  @   && txBufferLength == 0
  @   && transmitting   == 0;
  @*/
uint8_t TwoWire::endTransmission(void)
{
  return endTransmission(true);
}

/*
 * Must be called in: slave tx event callback
 * or after beginTransmission(address).
 */

/*@ requires
  @   txBufferIndex == txBufferLength;
  @ assigns
  @   txBuffer[0 .. BUFFER_LENGTH - 1],
  @   txBufferIndex,
  @   txBufferLength;
  @ behavior full:
  @   assumes
  @        transmitting != 0
  @     && txBufferLength >= BUFFER_LENGTH;
  @   assigns
  @     \nothing;
  @   ensures
  @     \result == 0;
  @ behavior master:
  @   assumes
  @        transmitting != 0
  @     && txBufferLength < BUFFER_LENGTH;
  @   assigns
  @     txBuffer[0 .. BUFFER_LENGTH - 1],
  @     txBufferIndex,
  @     txBufferLength;
  @   ensures
  @     \result == 1;
  @   ensures
  @        txBuffer[\old(txBufferIndex)] == \old(data)
  @     && txBufferIndex  == \old(txBufferIndex) + 1
  @     && txBufferLength == txBufferIndex;
  @ behavior slave:
  @   assumes
  @     transmitting == 0;
  @   assigns
  @     \nothing;
  @   ensures
  @     \result == 1;
  @ complete behaviors
  @   full, master, slave;
  @ disjoint behaviors
  @   full, master, slave;
  @*/
size_t TwoWire::write(uint8_t data)
{
  if (transmitting) { // master transmitter mode

    // don't bother if buffer is full
    if (txBufferLength >= BUFFER_LENGTH) {
      setWriteError();
      return 0;
    }

    // put byte in tx buffer
    txBuffer[txBufferIndex] = data;
    ++txBufferIndex;

    // update amount in buffer
    txBufferLength = txBufferIndex;

  } else { // slave send mode
    
    // reply to master
    twi_transmit(&data, 1);
  }

  return 1;
}

/*
 * Must be called in: slave tx event callback
 * or after beginTransmission(address).
 */

/*@ requires
  @      \valid(data + (0 .. quantity - 1))
  @   && 0 < quantity <= BUFFER_LENGTH
  @   && txBufferIndex == txBufferLength;
  @ assigns
  @   txBuffer[0 .. BUFFER_LENGTH - 1],
  @   txBufferIndex,
  @   txBufferLength;
  @ ensures
  @   \result == \old(quantity);
  @ behavior master:
  @   assumes
  @     transmitting != 0;
  @   assigns
  @     txBuffer[0 .. BUFFER_LENGTH - 1],
  @     txBufferIndex,
  @     txBufferLength;
  @   ensures
  @     txBufferIndex == txBufferLength;
  @ behavior slave:
  @   assumes
  @     transmitting == 0;
  @   assigns
  @     \nothing;
  @ complete behaviors
  @   master, slave;
  @ disjoint behaviors
  @   master, slave;
  @*/
size_t TwoWire::write(const uint8_t * data, size_t quantity)
{
  if (transmitting) { // master transmitter mode

    /*@ loop invariant
      @      0 <= i <= quantity
      @   && txBufferIndex == txBufferLength;
      @ loop assigns
      @   i, txBuffer[0 .. BUFFER_LENGTH - 1],
      @   txBufferIndex,
      @   txBufferLength;
      @ loop variant
      @   (quantity - i);
      @*/
    for (size_t i = 0; i < quantity; ++i) {
      TwoWire::write(data[i]);
    }

  } else { // slave send mode

    // reply to master
    twi_transmit(data, quantity);
  }

  return quantity;
}

/*
 * Must be called in: slave rx event callback
 * or after requestFrom(address, numBytes).
 */

/*@ requires
  @      0 <= rxBufferIndex <= rxBufferLength
  @   && 0 <= rxBufferLength <= BUFFER_LENGTH;
  @ assigns
  @   \nothing;
  @ ensures
  @   0 <= \result <= BUFFER_LENGTH;
  @*/
int TwoWire::available(void)
{
  return rxBufferLength - rxBufferIndex;
}

/*
 * Must be called in: slave rx event callback
 * or after requestFrom(address, numBytes).
 */

/*@ requires
  @   0 <= rxBufferLength <= BUFFER_LENGTH;
  @ assigns
  @   rxBufferIndex;
  @ behavior success:
  @   assumes
  @     rxBufferIndex < rxBufferLength;
  @   assigns
  @     rxBufferIndex;
  @   ensures
  @     0 <= \result <= 255;
  @   ensures
  @     rxBufferIndex == \old(rxBufferIndex) + 1;
  @ behavior error:
  @   assumes
  @     rxBufferIndex >= rxBufferLength;
  @   assigns
  @     \nothing;
  @   ensures
  @     \result == -1;
  @ complete behaviors
  @   success, error;
  @ disjoint behaviors
  @   success, error;
  @*/
int TwoWire::read(void)
{
  int value = -1;

  // get each successive byte on each call
  if (rxBufferIndex < rxBufferLength) {
    value = rxBuffer[rxBufferIndex];
    ++rxBufferIndex;
  }

  return value;
}

/*
 * Must be called in: slave rx event callback
 * or after requestFrom(address, numBytes).
 */

/*@ requires
  @   0 <= rxBufferLength <= BUFFER_LENGTH;
  @ assigns
  @   \nothing;
  @ behavior success:
  @   assumes
  @     rxBufferIndex < rxBufferLength;
  @   assigns
  @     \nothing;
  @   ensures
  @     0 <= \result <= 255;
  @ behavior error:
  @   assumes
  @     rxBufferIndex >= rxBufferLength;
  @   assigns
  @     \nothing;
  @   ensures
  @     \result == -1;
  @ complete behaviors
  @   success, error;
  @ disjoint behaviors
  @   success, error;
  @*/
int TwoWire::peek(void)
{
  int value = -1;

  if (rxBufferIndex < rxBufferLength) {
    value = rxBuffer[rxBufferIndex];
  }

  return value;
}

// To be implemented.
void TwoWire::flush(void) {}

// Behind the scenes function that is called when data is received.

/*@ requires
  @      \valid(inBytes + (0 .. numBytes - 1))
  @   && 0 < numBytes <= BUFFER_LENGTH;
  @ assigns
  @   rxBuffer[0 .. BUFFER_LENGTH - 1],
  @   rxBufferIndex,
  @   rxBufferLength;
  @ behavior success:
  @   assumes
  @        user_onReceive != NULL
  @     && rxBufferIndex >= rxBufferLength;
  @   assigns
  @     rxBuffer[0 .. BUFFER_LENGTH - 1],
  @     rxBufferIndex,
  @     rxBufferLength;
  @   ensures
  @        rxBufferIndex  == 0
  @     && rxBufferLength == \old(numBytes)
  @     && \forall uint8_t index ; 0 <= index < \old(numBytes) ==> rxBuffer[index] == inBytes[index];
  @ behavior error:
  @   assumes
  @         user_onReceive == NULL
  @     || (user_onReceive != NULL && rxBufferIndex < rxBufferLength);
  @   assigns
  @     \nothing;
  @ complete behaviors
  @   success, error;
  @ disjoint behaviors
  @   success, error;
  @*/
void TwoWire::onReceiveService(uint8_t * inBytes, int numBytes)
{
  // don't bother if user hasn't registered a callback
  if (user_onReceive == NULL) {
    return ;
  }

  /*
   * Don't bother if rx buffer is in use by a master requestFrom().
   * I know this drops data, but it allows for slight stupidity
   * meaning, they may not have read all the master requestFrom() data yet.
   */

  if (rxBufferIndex < rxBufferLength) {
    return ;
  }

  /*
   * Copy twi rx buffer into local read buffer.
   * This enables new reads to happen in parallel.
   */

  /*@ loop invariant
    @      0 <= i <= numBytes
    @   && \forall uint8_t k ; 0 <= k < i ==> rxBuffer[k] == inBytes[k];
    @ loop assigns
    @   i, rxBuffer[0 .. BUFFER_LENGTH - 1];
    @ loop variant
    @   (numBytes - i);
    @*/
  for (uint8_t i = 0; i < numBytes; ++i) {
    rxBuffer[i] = inBytes[i];
  }

  // set rx buffer iterator variables
  rxBufferIndex = 0;
  rxBufferLength = numBytes;

  // alert user program
  user_onReceive(numBytes);
}

// Behind the scenes function that is called when data is requested.

/*@ assigns
  @   txBufferIndex,
  @   txBufferLength;
  @ behavior success:
  @   assumes
  @     user_onRequest != NULL;
  @   assigns
  @     txBufferIndex,
  @     txBufferLength;
  @   ensures
  @        txBufferIndex  == 0
  @     && txBufferLength == 0;
  @ behavior error:
  @   assumes
  @     user_onRequest == NULL;
  @   assigns
  @     \nothing;
  @ complete behaviors
  @   success, error;
  @ disjoint behaviors
  @   success, error;
  @*/
void TwoWire::onRequestService(void)
{
  // don't bother if user hasn't registered a callback
  if (user_onRequest == NULL) {
    return ;
  }

  /*
   * reset tx buffer iterator variables
   * WARNING: This will kill any pending pre-master sendTo() activity.
   */

  txBufferIndex = 0;
  txBufferLength = 0;

  // alert user program
  user_onRequest();
}

// Sets function called on slave write.

/*@ requires
  @   \valid_function(function);
  @ assigns
  @   user_onReceive;
  @ ensures
  @   user_onReceive == \old(function);
  @*/
void TwoWire::onReceive(void (* function)(int))
{
  user_onReceive = function;
}

// Sets function called on slave read.

/*@ requires
  @   \valid_function(function);
  @ assigns
  @   user_onRequest;
  @ ensures
  @   user_onRequest == \old(function);
  @*/
void TwoWire::onRequest(void (* function)(void))
{
  user_onRequest = function;
}

// Pre Instantiate Objects

TwoWire Wire = TwoWire();
