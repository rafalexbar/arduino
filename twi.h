/*
 * twi.h - TWI/I2C Library for Wiring & Arduino
 * Copyright (c) 2006 Nicholas Zambetti. All rights reserved.
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 * 
 * Modified 2012 by Todd Krein (todd@krein.org) to implement repeated starts.
 */

#ifndef twi_h
#define twi_h

  #include <inttypes.h>

  // #define ATMEGA8

  #ifndef TWI_FREQ
  #define TWI_FREQ 100000L
  #endif

  #ifndef TWI_BUFFER_LENGTH
  #define TWI_BUFFER_LENGTH 32
  #endif

  #define TWI_READY 0
  #define TWI_MRX   1
  #define TWI_MTX   2
  #define TWI_SRX   3
  #define TWI_STX   4
  
  /* 
   * Function    twi_init
   * Description readys twi pins and sets twi bit rate
   * Input       none
   * Output      none
   */
  void twi_init(void);

  /* 
   * Function    twi_disable
   * Description disables twi pins
   * Input       none
   * Output      none
   */
  void twi_disable(void);

  /* 
   * Function    twi_slaveInit
   * Description sets slave address and enables interrupt
   * Input       none
   * Output      none
   */
  void twi_setAddress(uint8_t);

  /* 
   * Function    twi_setClock
   * Description sets twi bit rate
   * Input       clock frequency
   * Output      none
   */
  void twi_setFrequency(uint32_t);

  /* 
   * Function    twi_readFrom
   * Description attempts to become twi bus master and read a
   *             series of bytes from a device on the bus
   * Input       address  : 7-bit i2c device address
   *             data     : pointer to byte array
   *             length   : number of bytes to read into array
   *             sendStop : boolean indicating whether to send a stop at the end
   * Output      number of bytes read
   */
  uint8_t twi_readFrom(uint8_t, uint8_t *, uint8_t, uint8_t);

  /* 
   * Function    twi_writeTo
   * Description attempts to become twi bus master and write a
   *             series of bytes to a device on the bus
   * Input       address  : 7-bit i2c device address
   *             data     : pointer to byte array
   *             length   : number of bytes in array
   *             wait     : boolean indicating to wait for write or not
   *             sendStop : boolean indicating whether or not to send a stop at the end
   * Output      0 .. success
   *             1 .. length to long for buffer
   *             2 .. address send, nack received
   *             3 .. data send, nack received
   *             4 .. other twi error (lost bus arbitration, bus error, etc.)
   */
  uint8_t twi_writeTo(uint8_t, uint8_t *, uint8_t, uint8_t, uint8_t);

  /* 
   * Function    twi_transmit
   * Description fills slave tx buffer with data
   *             must be called in slave tx event callback
   * Input       data   : pointer to byte array
   *             length : number of bytes in array
   * Output      0 .. success
   *             1 .. length too long for buffer
   *             2 .. not slave transmitter
   */
  uint8_t twi_transmit(const uint8_t *, uint8_t);

  /* 
   * Function    twi_attachSlaveRxEvent
   * Description sets function called before a slave read operation
   * Input       function : callback function to use
   * Output      none
   */
  void twi_attachSlaveRxEvent(void (*)(uint8_t *, int));

  /* 
   * Function    twi_attachSlaveTxEvent
   * Description sets function called before a slave write operation
   * Input       function : callback function to use
   * Output      none
   */
  void twi_attachSlaveTxEvent(void (*)(void));

  /* 
   * Function    twi_reply
   * Description sends byte or readys receive line
   * Input       ack : byte indicating to ack or to nack
   * Output      none
   */
  void twi_reply(uint8_t);

  /* 
   * Function    twi_stop
   * Description relinquishes bus master status
   * Input       none
   * Output      none
   */
  void twi_stop(void);

  /* 
   * Function    twi_releaseBus
   * Description releases bus control
   * Input       none
   * Output      none
   */
  void twi_releaseBus(void);

#endif
