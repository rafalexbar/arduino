class Rectangle
{
  //@ class invariant positive = width > 0 && height > 0;

  //@ predicate bigger(Rectangle * r1, Rectangle * r2) = r1->width * r1->height > r2->width * r2->height;

  private:
  
    int width, height;
  
  public:
  
    Rectangle(int, int);
  
    int area(void);

    int difference(Rectangle * r);
  
    virtual int undefined(void);
};

/*@ requires
  @      0 < w <= 10
  @   && 0 < h <= 10;
  @ assigns
  @   width,
  @   height;
  @ ensures
  @      width  == \old(w)
  @   && height == \old(h);
  @*/
Rectangle::Rectangle(int w, int h)
{
  width  = w;
  height = h;
}

/*@ requires
  @      0 < width  <= 10
  @   && 0 < height <= 10;
  @ assigns
  @   \nothing;
  @ ensures
  @   \result == \old(width) * \old(height);
  @*/
int Rectangle::area(void)
{
  return width * height;
}

/*@ requires
  @      0 < width  <= 10
  @   && 0 < height <= 10
  @   && 0 < r->width  <= 10
  @   && 0 < r->height <= 10
  @   && \valid(r)
  @   && bigger(this, r);
  @ assigns
  @   \nothing;
  @ ensures
  @   \result == \old(width) * \old(height) - \old(r->width) * \old(r->height);
  @*/
int Rectangle::difference(Rectangle * r)
{
  return area() - r->area();
}

/*@ assigns
  @   \nothing;
  @ ensures
  @   \result == 0;
  @*/
int Rectangle::undefined(void)
{
  return 0;
}

/*@ assigns
  @   \nothing;
  @ ensures
  @   \result == 0;
  @*/
int main(int argc, char * argv[])
{
  Rectangle rectangleOne(4, 5);

  int areaOne = rectangleOne.area();

  Rectangle rectangleTwo(3, 4);

  int areaTwo = rectangleTwo.area();

  //@ assert areaOne > areaTwo;
  
  int difference = rectangleOne.difference(&rectangleTwo);

  //@ assert difference == 8;

  return 0;
}
