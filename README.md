# Functional Verification of Arduino Controllers

Wire Library Verification and Frama-Clang Plug-in Exploration.

## Configuration

### Requirements

* [Arduino IDE](https://www.arduino.cc/en/Main/Software)

* [Frama-C](https://frama-c.com/download.html)

* [Frama-Clang](https://frama-c.com/frama-clang.html)

`$ mkdir safe-wire && cd safe-wire`

`$ git clone https://rafalexbar@bitbucket.org/rafalexbar/arduino.git`

`$ cp -R /Applications/Arduino.app/Contents/Java/hardware .`

Rename the `inttypes.c` and `inttypes.h` files present in the `$(frama-c -print-path)/libc` folder to `inttypes-backup.c` and `inttypes-backup.h`.

Change the `global()` function declaration present in the `$(frama-c -print-path)/wp/ergo/Memory.mlw` file to:

`logic global_base : int`

`function global(b : int) : addr = { base = global_base + b; offset = 0 }`

## Execution

Run the following command from the `safe-wire` folder:

`$ frama-c-gui -wp -wp-rte -wp-no-volatile -cpp-extra-args="-D __AVR_ATmega328P__ -D __AVR_DEVICE_NAME__=atmega328p -D F_CPU=16000000L -I hardware/tools/avr/avr/include -I hardware/arduino/avr/cores/arduino -I hardware/arduino/avr/variants/standard" arduino/Wire.cpp arduino/twi.c`
