/*
 * twi.c - TWI/I2C Library for Wiring & Arduino
 * Copyright (c) 2006 Nicholas Zambetti. All rights reserved.
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 * 
 * Modified 2012 by Todd Krein (todd@krein.org) to implement repeated starts.
 */

#include <math.h>
#include <stdlib.h>
#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <compat/twi.h>
#include "Arduino.h" // for digitalWrite

#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif

#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

#include "pins_arduino.h"
#include "twi.h"

static volatile uint8_t twi_state;
static volatile uint8_t twi_slarw;
static volatile uint8_t twi_sendStop; // should the transaction end with a stop
static volatile uint8_t twi_inRepStart; // in the middle of a repeated start

static void (* twi_onSlaveTransmit)(void);
static void (* twi_onSlaveReceive)(uint8_t *, int);

static uint8_t twi_masterBuffer[TWI_BUFFER_LENGTH];
static volatile uint8_t twi_masterBufferIndex;
static volatile uint8_t twi_masterBufferLength;

static uint8_t twi_txBuffer[TWI_BUFFER_LENGTH];
static volatile uint8_t twi_txBufferIndex;
static volatile uint8_t twi_txBufferLength;

static uint8_t twi_rxBuffer[TWI_BUFFER_LENGTH];
static volatile uint8_t twi_rxBufferIndex;

static volatile uint8_t twi_error;

static uint8_t auxiliary_state;
static uint8_t auxiliary_slarw;
static uint8_t auxiliary_sendStop; // should the transaction end with a stop
static uint8_t auxiliary_inRepStart; // in the middle of a repeated start

// static void (* auxiliary_onSlaveTransmit)(void);
// static void (* auxiliary_onSlaveReceive)(uint8_t *, int);

// static uint8_t auxiliary_masterBuffer[TWI_BUFFER_LENGTH];
static uint8_t auxiliary_masterBufferIndex;
static uint8_t auxiliary_masterBufferLength;

// static uint8_t auxiliary_txBuffer[TWI_BUFFER_LENGTH];
static uint8_t auxiliary_txBufferIndex;
static uint8_t auxiliary_txBufferLength;

// static uint8_t auxiliary_rxBuffer[TWI_BUFFER_LENGTH];
static uint8_t auxiliary_rxBufferIndex;

static uint8_t auxiliary_error;

// TWAR == (* (volatile uint8_t *) (0xBA))
uint8_t auxiliary_TWAR;

// TWBR == (* (volatile uint8_t *) (0xB8))
uint8_t auxiliary_TWBR;

// TWCR == (* (volatile uint8_t *) (0xBC))
uint8_t auxiliary_TWCR;

// TWDR == (* (volatile uint8_t *) (0xBB))
uint8_t auxiliary_TWDR;

// TWSR == (* (volatile uint8_t *) (0xB9))
uint8_t auxiliary_TWSR;

/*@ axiomatic physical_address
  @ {
  @   axiom twar: \valid(&TWAR);
  @   axiom twbr: \valid(&TWBR);
  @   axiom twcr: \valid(&TWCR);
  @   axiom twdr: \valid(&TWDR);
  @   axiom twsr: \valid(&TWSR);
  @ }
  @*/

/*@ assigns
  @   auxiliary_state,
  @   twi_state,
  @   auxiliary_sendStop,
  @   twi_sendStop,
  @   auxiliary_inRepStart,
  @   twi_inRepStart,
  @   auxiliary_TWSR,
  @   TWSR,
  @   auxiliary_TWBR,
  @   TWBR,
  @   auxiliary_TWCR,
  @   TWCR;
  @ ensures
  @      (\forall uint8_t x ; (x == twi_state      && x == auxiliary_state)      ==> x == TWI_READY)
  @   && (\forall uint8_t x ; (x == twi_sendStop   && x == auxiliary_sendStop)   ==> x == true)
  @   && (\forall uint8_t x ; (x == twi_inRepStart && x == auxiliary_inRepStart) ==> x == false)
  @   && (\forall uint8_t x ; (x == TWBR && x == auxiliary_TWBR) ==> x == ((F_CPU / TWI_FREQ) - 16) / 2)
  @   && (\forall uint8_t x ; (x == TWCR && x == auxiliary_TWCR) ==> x == (_BV(TWEN) | _BV(TWIE) | _BV(TWEA)));
  @*/
void twi_init(void)
{
  // initialize state
  auxiliary_state = TWI_READY;
  twi_state = auxiliary_state;

  auxiliary_sendStop = true; // default value
  twi_sendStop = auxiliary_sendStop;

  auxiliary_inRepStart = false;
  twi_inRepStart = auxiliary_inRepStart;
  
  // activate internal pullups for twi
  digitalWrite(SDA, 1);
  digitalWrite(SCL, 1);

  // initialize twi prescaler and bit rate
  cbi(auxiliary_TWSR, TWPS0);
  //@ assert _SFR_BYTE(auxiliary_TWSR) == (\at(_SFR_BYTE(auxiliary_TWSR), Pre) & ~_BV(TWPS0));
  L1: cbi(auxiliary_TWSR, TWPS1);
  //@ assert _SFR_BYTE(auxiliary_TWSR) == (\at(_SFR_BYTE(auxiliary_TWSR), L1) & ~_BV(TWPS1));
  TWSR = auxiliary_TWSR;

  auxiliary_TWBR = ((F_CPU / TWI_FREQ) - 16) / 2;
  TWBR = auxiliary_TWBR;

  /*
   * twi bit rate formula from ATmega128 manual page 204
   * SCL Frequency = CPU Clock Frequency / (16 + (2 * TWBR))
   * Note: TWBR should be 10 or higher for master mode. It
   * is 72 for a 16 MHz Wiring board with 100 kHz TWI.
   */

  // enable twi module, acks and twi interrupt
  auxiliary_TWCR = _BV(TWEN) | _BV(TWIE) | _BV(TWEA);
  TWCR = auxiliary_TWCR;
}

/*@ assigns
  @   auxiliary_TWCR,
  @   TWCR;
  @ ensures
  @   \forall uint8_t x ; (x == TWCR && x == auxiliary_TWCR) ==> x == (\old(auxiliary_TWCR) & ~(_BV(TWEN) | _BV(TWIE) | _BV(TWEA)));
  @*/
void twi_disable(void)
{
  // disable twi module, acks and twi interrupt
  auxiliary_TWCR &= ~(_BV(TWEN) | _BV(TWIE) | _BV(TWEA));
  TWCR = auxiliary_TWCR;

  // deactivate internal pullups for twi
  digitalWrite(SDA, 0);
  digitalWrite(SCL, 0);
}

/*@ requires
  @   0 <= address < 128;
  @ assigns
  @   auxiliary_TWAR,
  @   TWAR;
  @ ensures
  @   \forall uint8_t x ; (x == TWAR && x == auxiliary_TWAR) ==> x == \old(address) << 1;
  @*/
void twi_setAddress(uint8_t address)
{
  // set twi slave address (skip over TWGCE bit)
  auxiliary_TWAR = address << 1;
  TWAR = auxiliary_TWAR;
}

/*@ requires
  @      frequency == 100000
  @   || frequency == 400000;
  @ assigns
  @   auxiliary_TWBR,
  @   TWBR;
  @ ensures
  @   \forall uint8_t x ; (x == TWBR && x == auxiliary_TWBR) ==> x == ((F_CPU / \old(frequency)) - 16) / 2;
  @*/
void twi_setFrequency(uint32_t frequency)
{
  auxiliary_TWBR = ((F_CPU / frequency) - 16) / 2;
  TWBR = auxiliary_TWBR;
  
  /*
   * twi bit rate formula from ATmega128 manual page 204
   * SCL Frequency = CPU Clock Frequency / (16 + (2 * TWBR))
   * Note: TWBR should be 10 or higher for master mode. It
   * is 72 for a 16 MHz Wiring board with 100 kHz TWI.
   */
}

/*@ requires
  @      0 <= address < 128
  @   && \valid(data + (0 .. length - 1))
  @   && 0 < length <= TWI_BUFFER_LENGTH
  @   && (sendStop == true || sendStop == false)
  @   && (\forall uint8_t index ; 0 <= index < length ==> data + index != &TWDR && data + index != &TWCR);
  @ assigns
  @   data[0 .. length - 1],
  @   auxiliary_state,
  @   twi_state,
  @   auxiliary_sendStop,
  @   twi_sendStop,
  @   auxiliary_error,
  @   twi_error,
  @   auxiliary_masterBufferIndex,
  @   twi_masterBufferIndex,
  @   auxiliary_masterBufferLength,
  @   twi_masterBufferLength,
  @   twi_masterBuffer[0 .. TWI_BUFFER_LENGTH - 1],
  @   auxiliary_slarw,
  @   twi_slarw,
  @   auxiliary_inRepStart,
  @   twi_inRepStart,
  @   auxiliary_TWDR,
  @   TWDR,
  @   auxiliary_TWCR,
  @   TWCR;
  @ behavior error:
  @   assumes
  @     TWI_BUFFER_LENGTH < length;
  @   assigns
  @     \nothing;
  @   ensures
  @     \result == 0;
  @ behavior success:
  @   assumes
  @     TWI_BUFFER_LENGTH >= length;
  @   assigns
  @     data[0 .. length - 1],
  @     auxiliary_state,
  @     twi_state,
  @     auxiliary_sendStop,
  @     twi_sendStop,
  @     auxiliary_error,
  @     twi_error,
  @     auxiliary_masterBufferIndex,
  @     twi_masterBufferIndex,
  @     auxiliary_masterBufferLength,
  @     twi_masterBufferLength,
  @     twi_masterBuffer[0 .. TWI_BUFFER_LENGTH - 1],
  @     auxiliary_slarw,
  @     twi_slarw,
  @     auxiliary_inRepStart,
  @     twi_inRepStart,
  @     auxiliary_TWDR,
  @     TWDR,
  @     auxiliary_TWCR,
  @     TWCR;
  @   ensures
  @     \result <= \old(length);
  @   ensures
  @        (\forall uint8_t x ; (x == twi_sendStop && x == auxiliary_sendStop) ==> x == \old(sendStop))
  @     && (\forall uint8_t x ; (x == twi_masterBufferLength && x == auxiliary_masterBufferLength) ==> x == \old(length) - 1)
  @     && (\forall uint8_t index ; 0 <= index < auxiliary_masterBufferIndex ==> data[index] == twi_masterBuffer[index]);
  @   ensures
  @     \forall uint8_t x ; (x == twi_slarw && x == auxiliary_slarw) ==> x == (TW_READ | \old(address) << 1);
  @ complete behaviors
  @   error, success;
  @ disjoint behaviors
  @   error, success;
  @*/
uint8_t twi_readFrom(uint8_t address, uint8_t * data, uint8_t length, uint8_t sendStop)
{
  // ensure data will fit into buffer
  if (TWI_BUFFER_LENGTH < length) {
    return 0;
  }

  // wait until twi is ready, become master receiver

  /*@ loop assigns
    @   auxiliary_state;
    @*/
  while (TWI_READY != auxiliary_state) {
    continue;
  }

  auxiliary_state = TWI_MRX;
  auxiliary_sendStop = sendStop;
  twi_sendStop = auxiliary_sendStop;
  // reset error state (0xFF .. no error occurred)
  auxiliary_error = 0xFF;

  // initialize buffer iteration variables
  auxiliary_masterBufferIndex = 0;
  auxiliary_masterBufferLength = length - 1;
  twi_masterBufferLength = auxiliary_masterBufferLength;
  
  /*
   * This is not intuitive, read on ...
   * On receive, the previously configured ACK/NACK setting is transmitted in
   * response to the received byte before the interrupt is signalled. 
   * Therefore, we must actually set NACK when the _next_ to last byte is
   * received, causing that NACK to be sent in response to receiving the last
   * expected byte of data.
   */

  // build sla+w, slave device address + w bit
  auxiliary_slarw = TW_READ;
  auxiliary_slarw |= address << 1;
  twi_slarw = auxiliary_slarw;

  if (true == auxiliary_inRepStart) {
    
    /*
     * If we're in the repeated start state, then we've already sent the start,
     * (@@@ we hope), and the TWI state machine is just waiting for the address byte.
     * We need to remove ourselves from the repeated start state before we enable interrupts,
     * since the ISR is ASYNC, and we could get confused if we hit the ISR before cleaning
     * up. Also, don't enable the START interrupt. There may be one pending from the 
     * repeated start that we sent ourselves, and that would really confuse things.
     */

    auxiliary_inRepStart = false; // remember, we're dealing with an ASYNC ISR
    
    /*@ loop assigns
      @   auxiliary_TWDR,
      @   auxiliary_TWCR;
      @*/
    do {
      auxiliary_TWDR = auxiliary_slarw;
    } while (auxiliary_TWCR & _BV(TWWC));

    // enable INTs, but not START
    auxiliary_TWCR = _BV(TWINT) | _BV(TWEA) | _BV(TWEN) | _BV(TWIE);
    
  } else {
    
    // send start condition
    auxiliary_TWCR = _BV(TWEN) | _BV(TWIE) | _BV(TWEA) | _BV(TWINT) | _BV(TWSTA);
  }

  // wait for read operation to complete

  /*@ loop assigns
    @   auxiliary_state;
    @*/
  while (TWI_MRX == auxiliary_state) {
    continue;
  }
  
  if (auxiliary_masterBufferIndex < length)
    length = auxiliary_masterBufferIndex;

  // copy twi buffer to data

  /*@ loop invariant
    @      0 <= i <= length
    @   && \forall uint8_t k ; 0 <= k < i ==> data[k] == twi_masterBuffer[k];
    @ loop assigns
    @   i, data[0 .. length - 1];
    @ loop variant
    @   (length - i);
    @*/
  for (uint8_t i = 0; i < length; ++i) {
    data[i] = twi_masterBuffer[i];
  }

  twi_state = auxiliary_state;
  twi_error = auxiliary_error;
  twi_masterBufferIndex = auxiliary_masterBufferIndex;
  twi_inRepStart = auxiliary_inRepStart;

  TWDR = auxiliary_TWDR;
  TWCR = auxiliary_TWCR;
	
  return length;
}

/*@ requires
  @      0 <= address < 128
  @   && \valid(data + (0 .. length - 1))
  @   && 0 < length <= TWI_BUFFER_LENGTH
  @   && (wait == true || wait == false)
  @   && (sendStop == true || sendStop == false)
  @   && (\forall uint8_t index ; 0 <= index < length ==> data + index != &TWDR && data + index != &TWCR);
  @ assigns
  @   auxiliary_state,
  @   twi_state,
  @   auxiliary_sendStop,
  @   twi_sendStop,
  @   auxiliary_error,
  @   twi_error,
  @   auxiliary_masterBufferIndex,
  @   twi_masterBufferIndex,
  @   auxiliary_masterBufferLength,
  @   twi_masterBufferLength,
  @   twi_masterBuffer[0 .. TWI_BUFFER_LENGTH - 1],
  @   auxiliary_slarw,
  @   twi_slarw,
  @   auxiliary_inRepStart,
  @   twi_inRepStart,
  @   auxiliary_TWDR,
  @   TWDR,
  @   auxiliary_TWCR,
  @   TWCR;
  @ behavior error:
  @   assumes
  @     TWI_BUFFER_LENGTH < length;
  @   assigns
  @     \nothing;
  @   ensures
  @     \result == 1;
  @ behavior regular:
  @   assumes
  @     TWI_BUFFER_LENGTH >= length;
  @   assigns
  @     auxiliary_state,
  @     twi_state,
  @     auxiliary_sendStop,
  @     twi_sendStop,
  @     auxiliary_error,
  @     twi_error,
  @     auxiliary_masterBufferIndex,
  @     twi_masterBufferIndex,
  @     auxiliary_masterBufferLength,
  @     twi_masterBufferLength,
  @     twi_masterBuffer[0 .. TWI_BUFFER_LENGTH - 1],
  @     auxiliary_slarw,
  @     twi_slarw,
  @     auxiliary_inRepStart,
  @     twi_inRepStart,
  @     auxiliary_TWDR,
  @     TWDR,
  @     auxiliary_TWCR,
  @     TWCR;
  @   ensures
  @     \forall uint8_t x ; (x == twi_error && x == auxiliary_error && x != 0xFF) ==> (\result == 2 || \result == 3 || \result == 4);
  @   ensures
  @     \forall uint8_t x ; (x == twi_error && x == auxiliary_error && x == 0xFF) ==> \result == 0;
  @   ensures
  @        (\forall uint8_t x ; (x == twi_sendStop && x == auxiliary_sendStop) ==> x == \old(sendStop))
  @     && (\forall uint8_t x ; (x == twi_masterBufferLength && x == auxiliary_masterBufferLength) ==> x == \old(length))
  @     && (\forall uint8_t index ; 0 <= index < \old(length) ==> twi_masterBuffer[index] == data[index]);
  @   ensures
  @     \forall uint8_t x ; (x == twi_slarw && x == auxiliary_slarw) ==> x == (TW_WRITE | \old(address) << 1);
  @ complete behaviors
  @   error, regular;
  @ disjoint behaviors
  @   error, regular;
  @*/
uint8_t twi_writeTo(uint8_t address, uint8_t * data, uint8_t length, uint8_t wait, uint8_t sendStop)
{
  // ensure data will fit into buffer
  if (TWI_BUFFER_LENGTH < length) {
    return 1;
  }

  // wait until twi is ready, become master transmitter
  
  /*@ loop assigns
    @   auxiliary_state;
    @*/
  while (TWI_READY != auxiliary_state) {
    continue;
  }

  auxiliary_state = TWI_MTX;
  auxiliary_sendStop = sendStop;
  twi_sendStop = auxiliary_sendStop;
  // reset error state (0xFF .. no error occurred)
  auxiliary_error = 0xFF;

  // initialize buffer iteration variables
  auxiliary_masterBufferIndex = 0;
  auxiliary_masterBufferLength = length;
  twi_masterBufferLength = auxiliary_masterBufferLength;
  
  // copy data to twi buffer

  /*@ loop invariant
    @      0 <= i <= length
    @   && \forall uint8_t k ; 0 <= k < i ==> twi_masterBuffer[k] == data[k];
    @ loop assigns
    @   i, twi_masterBuffer[0 .. TWI_BUFFER_LENGTH - 1];
    @ loop variant
    @   (length - i);
    @*/
  for (uint8_t i = 0; i < length; ++i) {
    twi_masterBuffer[i] = data[i];
  }
  
  // build sla+w, slave device address + w bit
  auxiliary_slarw = TW_WRITE;
  auxiliary_slarw |= address << 1;
  twi_slarw = auxiliary_slarw;
  
  /*
   * If we're in a repeated start, then we've already sent the START
   * in the ISR. Don't do it again.
   */

  if (true == auxiliary_inRepStart) {
    
    /*
     * If we're in the repeated start state, then we've already sent the start,
     * (@@@ we hope), and the TWI state machine is just waiting for the address byte.
     * We need to remove ourselves from the repeated start state before we enable interrupts,
     * since the ISR is ASYNC, and we could get confused if we hit the ISR before cleaning
     * up. Also, don't enable the START interrupt. There may be one pending from the 
     * repeated start that we sent ourselves, and that would really confuse things.
     */

    auxiliary_inRepStart = false; // remember, we're dealing with an ASYNC ISR
    
    /*@ loop assigns
      @   auxiliary_TWDR,
      @   auxiliary_TWCR;
      @*/
    do {
      auxiliary_TWDR = auxiliary_slarw;
    } while (auxiliary_TWCR & _BV(TWWC));
    
    // enable INTs, but not START
    auxiliary_TWCR = _BV(TWINT) | _BV(TWEA) | _BV(TWEN) | _BV(TWIE);
    
  } else {
    
    // send start condition
    auxiliary_TWCR = _BV(TWINT) | _BV(TWEA) | _BV(TWEN) | _BV(TWIE) | _BV(TWSTA);
  }

  // wait for write operation to complete

  /*@ loop assigns
    @   auxiliary_state;
    @*/
  while (wait && (TWI_MTX == auxiliary_state)) {
    continue;
  }

  twi_state = auxiliary_state;
  twi_error = auxiliary_error;
  twi_masterBufferIndex = auxiliary_masterBufferIndex;
  twi_inRepStart = auxiliary_inRepStart;

  TWDR = auxiliary_TWDR;
  TWCR = auxiliary_TWCR;

  if (auxiliary_error == 0xFF) {
    return 0; // success
  } else if (auxiliary_error == TW_MT_SLA_NACK) {
    return 2; // error: address send, nack received
  } else if (auxiliary_error == TW_MT_DATA_NACK) {
    return 3; // error: data send, nack received
  } else {
    return 4; // other twi error
  }
}

/*@ requires
  @      \valid(data + (0 .. length - 1))
  @   && 0 < length <= TWI_BUFFER_LENGTH;
  @ assigns
  @   twi_txBuffer[0 .. TWI_BUFFER_LENGTH - 1],
  @   auxiliary_txBufferLength,
  @   twi_txBufferLength;
  @ behavior success:
  @   assumes
  @        TWI_BUFFER_LENGTH >= (auxiliary_txBufferLength + length)
  @     && TWI_STX == auxiliary_state;
  @   assigns
  @     twi_txBuffer[0 .. TWI_BUFFER_LENGTH - 1],
  @     auxiliary_txBufferLength,
  @     twi_txBufferLength;
  @   ensures
  @     \result == 0;
  @   ensures
  @        (\forall uint8_t x ; (x == twi_txBufferLength && x == auxiliary_txBufferLength) ==> x == \old(auxiliary_txBufferLength) + \old(length))
  @     && (\forall uint8_t index ; 0 <= index < \old(length) ==> twi_txBuffer[\old(auxiliary_txBufferLength) + index] == data[index]);
  @ behavior full:
  @   assumes
  @     TWI_BUFFER_LENGTH < (auxiliary_txBufferLength + length);
  @   assigns
  @     \nothing;
  @   ensures
  @     \result == 1;
  @ behavior receiver:
  @   assumes
  @        TWI_BUFFER_LENGTH >= (auxiliary_txBufferLength + length)
  @     && TWI_STX != auxiliary_state;
  @   assigns
  @     \nothing;
  @   ensures
  @     \result == 2;
  @ complete behaviors
  @   success, full, receiver;
  @ disjoint behaviors
  @   success, full, receiver;
  @*/
uint8_t twi_transmit(const uint8_t * data, uint8_t length)
{
  // ensure data will fit into buffer
  if (TWI_BUFFER_LENGTH < (auxiliary_txBufferLength + length)) {
    return 1;
  }
  
  // ensure we are currently a slave transmitter
  if (TWI_STX != auxiliary_state) {
    return 2;
  }
  
  // set length and copy data into tx buffer
  
  /*@ loop invariant
    @      0 <= i <= length
    @   && \forall uint8_t k ; 0 <= k < i ==> twi_txBuffer[auxiliary_txBufferLength + k] == data[k];
    @ loop assigns
    @   i, twi_txBuffer[0 .. TWI_BUFFER_LENGTH - 1];
    @ loop variant
    @   (length - i);
    @*/
  for (uint8_t i = 0; i < length; ++i) {
    twi_txBuffer[auxiliary_txBufferLength + i] = data[i];
  }

  auxiliary_txBufferLength += length;
  twi_txBufferLength = auxiliary_txBufferLength;
  
  return 0;
}

/*@ requires
  @   \valid_function(function);
  @ assigns
  @   twi_onSlaveReceive;
  @ ensures
  @   twi_onSlaveReceive == \old(function);
  @*/
void twi_attachSlaveRxEvent(void (* function)(uint8_t *, int))
{
  twi_onSlaveReceive = function;
}

/*@ requires
  @   \valid_function(function);
  @ assigns
  @   twi_onSlaveTransmit;
  @ ensures
  @   twi_onSlaveTransmit == \old(function);
  @*/
void twi_attachSlaveTxEvent(void (* function)(void))
{
  twi_onSlaveTransmit = function;
}

/*@ requires
  @      ack == true
  @   || ack == false;
  @ assigns
  @   auxiliary_TWCR,
  @   TWCR;
  @ behavior with:
  @   assumes
  @     ack == true;
  @   assigns
  @     auxiliary_TWCR,
  @     TWCR;
  @   ensures
  @     \forall uint8_t x ; (x == TWCR && x == auxiliary_TWCR) ==> x == (_BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA));
  @ behavior without:
  @   assumes
  @     ack == false;
  @   assigns
  @     auxiliary_TWCR,
  @     TWCR;
  @   ensures
  @     \forall uint8_t x ; (x == TWCR && x == auxiliary_TWCR) ==> x == (_BV(TWEN) | _BV(TWIE) | _BV(TWINT));
  @ complete behaviors
  @   with, without;
  @ disjoint behaviors
  @   with, without;
  @*/
void twi_reply(uint8_t ack)
{
  // transmit master read ready signal, with or without ack
  if (ack) {
    auxiliary_TWCR = _BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA);
  } else {
    auxiliary_TWCR = _BV(TWEN) | _BV(TWIE) | _BV(TWINT);
  }
  
  TWCR = auxiliary_TWCR;
}

/*@ assigns
  @   auxiliary_state,
  @   twi_state,
  @   auxiliary_TWCR,
  @   TWCR;
  @ ensures
  @   \forall uint8_t x ; (x == twi_state && x == auxiliary_state) ==> x == TWI_READY;
  @*/
void twi_stop(void)
{
  // send stop condition
  auxiliary_TWCR = _BV(TWEN) | _BV(TWIE) | _BV(TWEA) | _BV(TWINT) | _BV(TWSTO);

  /*
   * wait for stop condition to be executed on bus
   * Note: TWINT is not set after a stop condition!
   */

  /*@ loop assigns
    @   auxiliary_TWCR;
    @*/
  while (auxiliary_TWCR & _BV(TWSTO)) {
    continue;
  }

  TWCR = auxiliary_TWCR;

  // update twi state
  auxiliary_state = TWI_READY;
  twi_state = auxiliary_state;
}

/*@ assigns
  @   auxiliary_state,
  @   twi_state,
  @   auxiliary_TWCR,
  @   TWCR;
  @ ensures
  @      (\forall uint8_t x ; (x == twi_state && x == auxiliary_state) ==> x == TWI_READY)
  @   && (\forall uint8_t x ; (x == TWCR && x == auxiliary_TWCR) ==> x == (_BV(TWEN) | _BV(TWIE) | _BV(TWEA) | _BV(TWINT)));
  @*/
void twi_releaseBus(void)
{
  // release bus
  auxiliary_TWCR = _BV(TWEN) | _BV(TWIE) | _BV(TWEA) | _BV(TWINT);
  TWCR = auxiliary_TWCR;

  // update twi state
  auxiliary_state = TWI_READY;
  twi_state = auxiliary_state;
}

/*@ requires
  @      0 <= twi_masterBufferLength <= TWI_BUFFER_LENGTH
  @   && 0 <= twi_txBufferLength <= TWI_BUFFER_LENGTH;
  @ assigns
  @   twi_state,
  @   twi_inRepStart,
  @   twi_masterBuffer[0 .. TWI_BUFFER_LENGTH - 1],
  @   twi_masterBufferIndex,
  @   twi_txBuffer[0 .. TWI_BUFFER_LENGTH - 1],
  @   twi_txBufferIndex,
  @   twi_txBufferLength,
  @   twi_rxBuffer[0 .. TWI_BUFFER_LENGTH - 1],
  @   twi_rxBufferIndex,
  @   twi_error,
  @   TWDR,
  @   TWCR;
  @ behavior all_master:
  @   assumes
  @        TW_STATUS == TW_START
  @     || TW_STATUS == TW_REP_START;
  @   assigns
  @     TWDR,
  @     TWCR;
  @   ensures
  @        TWDR == \old(twi_slarw)
  @     && TWCR == (_BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA));
  @ behavior master_transmitter:
  @   assumes
  @        TW_STATUS == TW_MT_SLA_ACK
  @     || TW_STATUS == TW_MT_DATA_ACK
  @     || TW_STATUS == TW_MT_SLA_NACK
  @     || TW_STATUS == TW_MT_DATA_NACK
  @     || TW_STATUS == TW_MT_ARB_LOST;
  @   assigns
  @     twi_state,
  @     twi_inRepStart,
  @     twi_masterBufferIndex,
  @     twi_error,
  @     TWDR,
  @     TWCR;
  @   ensures
  @     ((\old(TW_STATUS) == TW_MT_SLA_ACK || \old(TW_STATUS) == TW_MT_DATA_ACK) && \old(twi_masterBufferIndex) < \old(twi_masterBufferLength)) ==>
  @       (TWDR == twi_masterBuffer[\old(twi_masterBufferIndex)] && twi_masterBufferIndex == \old(twi_masterBufferIndex) + 1 && TWCR == (_BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA)));
  @   ensures
  @     ((\old(TW_STATUS) == TW_MT_SLA_ACK || \old(TW_STATUS) == TW_MT_DATA_ACK) && \old(twi_masterBufferIndex) >= \old(twi_masterBufferLength) && \old(twi_sendStop) != 0) ==>
  @       twi_state == TWI_READY;
  @   ensures
  @     ((\old(TW_STATUS) == TW_MT_SLA_ACK || \old(TW_STATUS) == TW_MT_DATA_ACK) && \old(twi_masterBufferIndex) >= \old(twi_masterBufferLength) && \old(twi_sendStop) == 0) ==>
  @       (twi_inRepStart == true && TWCR == (_BV(TWINT) | _BV(TWSTA) | _BV(TWEN)) && twi_state == TWI_READY);
  @   ensures
  @     \old(TW_STATUS) == TW_MT_SLA_NACK ==>
  @       (twi_error == TW_MT_SLA_NACK && twi_state == TWI_READY);
  @   ensures
  @     \old(TW_STATUS) == TW_MT_DATA_NACK ==>
  @       (twi_error == TW_MT_DATA_NACK && twi_state == TWI_READY);
  @   ensures
  @     \old(TW_STATUS) == TW_MT_ARB_LOST ==>
  @       (twi_error == TW_MT_ARB_LOST && twi_state == TWI_READY && TWCR == (_BV(TWEN) | _BV(TWIE) | _BV(TWEA) | _BV(TWINT)));
  @ behavior master_receiver:
  @   assumes
  @        TW_STATUS == TW_MR_DATA_ACK
  @     || TW_STATUS == TW_MR_SLA_ACK
  @     || TW_STATUS == TW_MR_DATA_NACK
  @     || TW_STATUS == TW_MR_SLA_NACK;
  @   assigns
  @     twi_state,
  @     twi_inRepStart,
  @     twi_masterBuffer[0 .. TWI_BUFFER_LENGTH - 1],
  @     twi_masterBufferIndex,
  @     TWCR;
  @   ensures
  @     (\old(TW_STATUS) == TW_MR_DATA_ACK && \old(twi_masterBufferIndex) < \old(twi_masterBufferLength)) ==>
  @       (twi_masterBuffer[\old(twi_masterBufferIndex)] == \old(TWDR) && twi_masterBufferIndex == \old(twi_masterBufferIndex) + 1);
  @   ensures
  @     (\old(TW_STATUS) == TW_MR_DATA_ACK && twi_masterBufferIndex < \old(twi_masterBufferLength)) ==>
  @       TWCR == (_BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA));
  @   ensures
  @     (\old(TW_STATUS) == TW_MR_DATA_ACK && twi_masterBufferIndex >= \old(twi_masterBufferLength)) ==>
  @       TWCR == (_BV(TWEN) | _BV(TWIE) | _BV(TWINT));
  @   ensures
  @     (\old(TW_STATUS) == TW_MR_SLA_ACK && \old(twi_masterBufferIndex) < \old(twi_masterBufferLength)) ==>
  @       TWCR == (_BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA));
  @   ensures
  @     (\old(TW_STATUS) == TW_MR_SLA_ACK && \old(twi_masterBufferIndex) >= \old(twi_masterBufferLength)) ==>
  @       TWCR == (_BV(TWEN) | _BV(TWIE) | _BV(TWINT));
  @   ensures
  @     (\old(TW_STATUS) == TW_MR_DATA_NACK && \old(twi_masterBufferIndex) < TWI_BUFFER_LENGTH) ==>
  @       (twi_masterBuffer[\old(twi_masterBufferIndex)] == \old(TWDR) && twi_masterBufferIndex == \old(twi_masterBufferIndex) + 1);
  @   ensures
  @     (\old(TW_STATUS) == TW_MR_DATA_NACK && \old(twi_sendStop) != 0) ==>
  @       twi_state == TWI_READY;
  @   ensures
  @     (\old(TW_STATUS) == TW_MR_DATA_NACK && \old(twi_sendStop) == 0) ==>
  @       (twi_inRepStart == true && TWCR == (_BV(TWINT) | _BV(TWSTA) | _BV(TWEN)) && twi_state == TWI_READY);
  @   ensures
  @     \old(TW_STATUS) == TW_MR_SLA_NACK ==>
  @       twi_state == TWI_READY;
  @ behavior slave_receiver:
  @   assumes
  @        TW_STATUS == TW_SR_SLA_ACK
  @     || TW_STATUS == TW_SR_GCALL_ACK
  @     || TW_STATUS == TW_SR_ARB_LOST_SLA_ACK
  @     || TW_STATUS == TW_SR_ARB_LOST_GCALL_ACK
  @     || TW_STATUS == TW_SR_DATA_ACK
  @     || TW_STATUS == TW_SR_GCALL_DATA_ACK
  @     || TW_STATUS == TW_SR_STOP
  @     || TW_STATUS == TW_SR_DATA_NACK
  @     || TW_STATUS == TW_SR_GCALL_DATA_NACK;
  @   assigns
  @     twi_state,
  @     twi_rxBuffer[0 .. TWI_BUFFER_LENGTH - 1],
  @     twi_rxBufferIndex,
  @     TWCR;
  @   ensures
  @     (\old(TW_STATUS) == TW_SR_SLA_ACK || \old(TW_STATUS) == TW_SR_GCALL_ACK || \old(TW_STATUS) == TW_SR_ARB_LOST_SLA_ACK || \old(TW_STATUS) == TW_SR_ARB_LOST_GCALL_ACK) ==>
  @       (twi_state == TWI_SRX && twi_rxBufferIndex == 0 && TWCR == (_BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA)));
  @   ensures
  @     ((\old(TW_STATUS) == TW_SR_DATA_ACK || \old(TW_STATUS) == TW_SR_GCALL_DATA_ACK) && \old(twi_rxBufferIndex) < TWI_BUFFER_LENGTH) ==>
  @       (twi_rxBuffer[\old(twi_rxBufferIndex)] == \old(TWDR) && twi_rxBufferIndex == \old(twi_rxBufferIndex) + 1 && TWCR == (_BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA)));
  @   ensures
  @     ((\old(TW_STATUS) == TW_SR_DATA_ACK || \old(TW_STATUS) == TW_SR_GCALL_DATA_ACK) && \old(twi_rxBufferIndex) >= TWI_BUFFER_LENGTH) ==>
  @       TWCR == (_BV(TWEN) | _BV(TWIE) | _BV(TWINT));
  @   ensures
  @     (\old(TW_STATUS) == TW_SR_STOP && \old(twi_rxBufferIndex) < TWI_BUFFER_LENGTH) ==>
  @       (twi_state == TWI_READY && TWCR == (_BV(TWEN) | _BV(TWIE) | _BV(TWEA) | _BV(TWINT)) && twi_rxBuffer[\old(twi_rxBufferIndex)] == '\0' && twi_rxBufferIndex == 0);
  @   ensures
  @     (\old(TW_STATUS) == TW_SR_STOP && \old(twi_rxBufferIndex) >= TWI_BUFFER_LENGTH) ==>
  @       (twi_state == TWI_READY && TWCR == (_BV(TWEN) | _BV(TWIE) | _BV(TWEA) | _BV(TWINT)) && twi_rxBufferIndex == 0);
  @   ensures
  @     (\old(TW_STATUS) == TW_SR_DATA_NACK || \old(TW_STATUS) == TW_SR_GCALL_DATA_NACK) ==>
  @       TWCR == (_BV(TWEN) | _BV(TWIE) | _BV(TWINT));
  @ behavior slave_transmitter:
  @   assumes
  @        TW_STATUS == TW_ST_SLA_ACK
  @     || TW_STATUS == TW_ST_ARB_LOST_SLA_ACK
  @     || TW_STATUS == TW_ST_DATA_ACK
  @     || TW_STATUS == TW_ST_DATA_NACK
  @     || TW_STATUS == TW_ST_LAST_DATA;
  @   assigns
  @     twi_state,
  @     twi_txBuffer[0 .. TWI_BUFFER_LENGTH - 1],
  @     twi_txBufferIndex,
  @     twi_txBufferLength,
  @     TWDR,
  @     TWCR;
  @   ensures
  @     (\old(TW_STATUS) == TW_ST_SLA_ACK || \old(TW_STATUS) == TW_ST_ARB_LOST_SLA_ACK) ==>
  @       twi_state == TWI_STX;
  @   ensures
  @     ((\old(TW_STATUS) == TW_ST_SLA_ACK || \old(TW_STATUS) == TW_ST_ARB_LOST_SLA_ACK) && 0 < twi_txBufferLength) ==>
  @       (TWDR == twi_txBuffer[0] && twi_txBufferIndex == 1);
  @   ensures
  @     ((\old(TW_STATUS) == TW_ST_SLA_ACK || \old(TW_STATUS) == TW_ST_ARB_LOST_SLA_ACK) && twi_txBufferIndex < twi_txBufferLength) ==>
  @       TWCR == (_BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA));
  @   ensures
  @     ((\old(TW_STATUS) == TW_ST_SLA_ACK || \old(TW_STATUS) == TW_ST_ARB_LOST_SLA_ACK) && twi_txBufferIndex >= twi_txBufferLength) ==>
  @       TWCR == (_BV(TWEN) | _BV(TWIE) | _BV(TWINT));
  @   ensures
  @     (\old(TW_STATUS) == TW_ST_DATA_ACK && \old(twi_txBufferIndex) < \old(twi_txBufferLength)) ==>
  @       (TWDR == twi_txBuffer[\old(twi_txBufferIndex)] && twi_txBufferIndex == \old(twi_txBufferIndex) + 1);
  @   ensures
  @     (\old(TW_STATUS) == TW_ST_DATA_ACK && twi_txBufferIndex < \old(twi_txBufferLength)) ==>
  @       TWCR == (_BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA));
  @   ensures
  @     (\old(TW_STATUS) == TW_ST_DATA_ACK && twi_txBufferIndex >= \old(twi_txBufferLength)) ==>
  @       TWCR == (_BV(TWEN) | _BV(TWIE) | _BV(TWINT));
  @   ensures
  @     (\old(TW_STATUS) == TW_ST_DATA_NACK || \old(TW_STATUS) == TW_ST_LAST_DATA) ==>
  @       (TWCR == (_BV(TWEN) | _BV(TWIE) | _BV(TWINT) | _BV(TWEA)) && twi_state == TWI_READY);
  @ behavior all:
  @   assumes
  @        TW_STATUS == TW_NO_INFO
  @     || TW_STATUS == TW_BUS_ERROR;
  @   assigns
  @     twi_state,
  @     twi_error,
  @     TWCR;
  @   ensures
  @     \old(TW_STATUS) == TW_BUS_ERROR ==>
  @       (twi_error == TW_BUS_ERROR && twi_state == TWI_READY);
  @ behavior default:
  @   assumes
  @        TW_STATUS != TW_START
  @     && TW_STATUS != TW_REP_START
  @     && TW_STATUS != TW_MT_SLA_ACK
  @     && TW_STATUS != TW_MT_DATA_ACK
  @     && TW_STATUS != TW_MT_SLA_NACK
  @     && TW_STATUS != TW_MT_DATA_NACK
  @     && TW_STATUS != TW_MT_ARB_LOST
  @     && TW_STATUS != TW_MR_DATA_ACK
  @     && TW_STATUS != TW_MR_SLA_ACK
  @     && TW_STATUS != TW_MR_DATA_NACK
  @     && TW_STATUS != TW_MR_SLA_NACK
  @     && TW_STATUS != TW_SR_SLA_ACK
  @     && TW_STATUS != TW_SR_GCALL_ACK
  @     && TW_STATUS != TW_SR_ARB_LOST_SLA_ACK
  @     && TW_STATUS != TW_SR_ARB_LOST_GCALL_ACK
  @     && TW_STATUS != TW_SR_DATA_ACK
  @     && TW_STATUS != TW_SR_GCALL_DATA_ACK
  @     && TW_STATUS != TW_SR_STOP
  @     && TW_STATUS != TW_SR_DATA_NACK
  @     && TW_STATUS != TW_SR_GCALL_DATA_NACK
  @     && TW_STATUS != TW_ST_SLA_ACK
  @     && TW_STATUS != TW_ST_ARB_LOST_SLA_ACK
  @     && TW_STATUS != TW_ST_DATA_ACK
  @     && TW_STATUS != TW_ST_DATA_NACK
  @     && TW_STATUS != TW_ST_LAST_DATA
  @     && TW_STATUS != TW_NO_INFO
  @     && TW_STATUS != TW_BUS_ERROR;
  @   assigns
  @     \nothing;
  @ complete behaviors
  @   all_master, master_transmitter, master_receiver, slave_receiver, slave_transmitter, all, default;
  @ disjoint behaviors
  @   all_master, master_transmitter, master_receiver, slave_receiver, slave_transmitter, all, default;
  @*/
ISR(TWI_vect)
{
  switch (TW_STATUS)
  {
    // All Master
    case TW_START:     // sent start condition
    case TW_REP_START: // sent repeated start condition
      // copy device address and r/w bit to output register and ack
      TWDR = twi_slarw;
      twi_reply(1);
      break;
    
    // Master Transmitter
    case TW_MT_SLA_ACK:  // slave receiver acked address
    case TW_MT_DATA_ACK: // slave receiver acked data
      // if there is data to send, send it, otherwise stop
      if (twi_masterBufferIndex < twi_masterBufferLength) {
        // copy data to output register and ack
        TWDR = twi_masterBuffer[twi_masterBufferIndex++];
        twi_reply(1);
      } else if (twi_sendStop) {
        twi_stop();
      } else {
        twi_inRepStart = true; // we're gonna send the START
        /*
         * Don't enable the interrupt. We'll generate the start, but we
         * avoid handling the interrupt until we're in the next transaction,
         * at the point where we would normally issue the start.
         */
        TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);
        twi_state = TWI_READY;
      }
      break;
    case TW_MT_SLA_NACK:  // address sent, nack received
      twi_error = TW_MT_SLA_NACK;
      twi_stop();
      break;
    case TW_MT_DATA_NACK: // data sent, nack received
      twi_error = TW_MT_DATA_NACK;
      twi_stop();
      break;
    case TW_MT_ARB_LOST:  // lost bus arbitration
      twi_error = TW_MT_ARB_LOST;
      twi_releaseBus();
      break;
    
    // Master Receiver
    case TW_MR_DATA_ACK: // data received, ack sent
      // put byte into buffer
      if (twi_masterBufferIndex < twi_masterBufferLength) {
        twi_masterBuffer[twi_masterBufferIndex++] = TWDR;
      }
    case TW_MR_SLA_ACK:  // address sent, ack received
      // ack if more bytes are expected, otherwise nack
      if (twi_masterBufferIndex < twi_masterBufferLength) {
        twi_reply(1);
      } else {
        twi_reply(0);
      }
      break;
    case TW_MR_DATA_NACK: // data received, nack sent
      // put final byte into buffer
      if (twi_masterBufferIndex < TWI_BUFFER_LENGTH) {
        twi_masterBuffer[twi_masterBufferIndex++] = TWDR;
      }
      if (twi_sendStop) {
        twi_stop();
      } else {
        twi_inRepStart = true; // we're gonna send the START
        /*
         * Don't enable the interrupt. We'll generate the start, but we 
         * avoid handling the interrupt until we're in the next transaction,
         * at the point where we would normally issue the start.
         */
        TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN);
        twi_state = TWI_READY;
      }
      break;
    case TW_MR_SLA_NACK: // address sent, nack received
      twi_stop();
      break;
    // TW_MR_ARB_LOST handled by TW_MT_ARB_LOST case
    
    // Slave Receiver
    case TW_SR_SLA_ACK:   // addressed, returned ack
    case TW_SR_GCALL_ACK: // addressed generally, returned ack
    case TW_SR_ARB_LOST_SLA_ACK:   // lost arbitration, returned ack
    case TW_SR_ARB_LOST_GCALL_ACK: // lost arbitration, returned ack
      // enter slave receiver mode
      twi_state = TWI_SRX;
      // indicate that rx buffer can be overwritten and ack
      twi_rxBufferIndex = 0;
      twi_reply(1);
      break;
    case TW_SR_DATA_ACK:       // data received, returned ack
    case TW_SR_GCALL_DATA_ACK: // data received generally, returned ack
      // if there is still room in the rx buffer
      if (twi_rxBufferIndex < TWI_BUFFER_LENGTH) {
        // put byte in buffer and ack
        twi_rxBuffer[twi_rxBufferIndex++] = TWDR;
        twi_reply(1);
      } else {
        // otherwise nack
        twi_reply(0);
      }
      break;
    case TW_SR_STOP: // stop or repeated start condition received
      // ack future responses and leave slave receiver state
      twi_releaseBus();
      // put a null char after data if there's room
      if (twi_rxBufferIndex < TWI_BUFFER_LENGTH) {
        twi_rxBuffer[twi_rxBufferIndex] = '\0';
      }
      // callback to user defined callback
      twi_onSlaveReceive(twi_rxBuffer, twi_rxBufferIndex);
      // since we submit rx buffer to "wire" library, we can reset it
      twi_rxBufferIndex = 0;
      break;
    case TW_SR_DATA_NACK:       // data received, returned nack
    case TW_SR_GCALL_DATA_NACK: // data received generally, returned nack
      // nack back at master
      twi_reply(0);
      break;
    
    // Slave Transmitter
    case TW_ST_SLA_ACK:          // addressed, returned ack
    case TW_ST_ARB_LOST_SLA_ACK: // arbitration lost, returned ack
      // enter slave transmitter mode
      twi_state = TWI_STX;
      // ready the tx buffer index for iteration
      twi_txBufferIndex = 0;
      // set tx buffer length to be zero, to verify if user changes it
      twi_txBufferLength = 0;
      // request for txBuffer to be filled and length to be set
      // Note: User must call twi_transmit(bytes, length) to do this.
      twi_onSlaveTransmit();
      // if they didn't change buffer and length, initialize it
      if (twi_txBufferLength == 0) {
        twi_txBufferLength = 1;
        twi_txBuffer[0] = 0x00;
      }
      // transmit first byte from buffer, fall
    case TW_ST_DATA_ACK: // byte sent, ack returned
      // copy data to output register
      if (twi_txBufferIndex < twi_txBufferLength) {
        TWDR = twi_txBuffer[twi_txBufferIndex++];
      }
      // if there is more to send, ack, otherwise nack
      if (twi_txBufferIndex < twi_txBufferLength) {
        twi_reply(1);
      } else {
        twi_reply(0);
      }
      break;
    case TW_ST_DATA_NACK: // received nack, we are done!
    case TW_ST_LAST_DATA: // received ack, but we are done already!
      // ack future responses
      twi_reply(1);
      // leave slave receiver state
      twi_state = TWI_READY;
      break;
    
    // All
    case TW_NO_INFO:   // no state information
      break;
    case TW_BUS_ERROR: // bus error, illegal stop/start
      twi_error = TW_BUS_ERROR;
      twi_stop();
      break;
  }
}
